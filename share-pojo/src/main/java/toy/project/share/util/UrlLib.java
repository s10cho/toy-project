package toy.project.share.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UrlLib {
    public static String decode(String value) {
        try {
            return URLDecoder.decode(value, String.valueOf(StandardCharsets.UTF_8));
        } catch (UnsupportedEncodingException | NullPointerException e) {
            return value;
        }
    }

    public static List<String> decode(String[] values) {
        List<String> result = new ArrayList<>();
        for (String value : values) {
            if (StringUtils.isNotBlank(value)) {
                result.add(decode(value));
            }
        }
        return result;
    }
}
