package toy.project.share.util;

public class SamplePrinter {

    @SuppressWarnings("squid:S106")
    public static void println(String message) {
        System.out.println(message);
    }

    public static void println(Object object) {
        println(JsonUtil.toJson(object));
    }

    public static void main(String[] args) {
        println("test");
    }
}
