package toy.project.share.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonSerializable;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.StreamLib;

@NoArgsConstructor
public class IdSet extends LinkedHashSet<String> {
    public IdSet(Collection<? extends String> c) {
        super(c);
    }

    public static IdSet fromJson(String json) {
        return JsonUtil.fromJson(json, IdSet.class);
    }

    public static IdSet empty() {
        return new IdSet();
    }

    public static IdSet of(String... ids) {
        return new IdSet(Arrays.asList(ids));
    }

    public static IdSet sample() {
        return IdSet.of("Doris", "Craig", "Young");
    }

    public IdSet with(String id) {
        if (StringUtils.isNotBlank(id)) {
            add(id);
        }
        return this;
    }

    public IdSet filter(IdSet filter) {
        return filter.isEmpty() ? this : filter.stream().filter(this::contains).collect(Collectors.toCollection(IdSet::new));
    }

    public IdSet without(IdSet without) {
        return stream()
            .filter(StreamLib.not(without::contains))
            .collect(Collectors.toCollection(IdSet::new));
    }

    public IdSet without(String without) {
        return stream()
            .filter(StreamLib.not(without::equals))
            .collect(Collectors.toCollection(IdSet::new));
    }

    @JsonIgnore
    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public IdList toList() {
        return new IdList(this);
    }
}
