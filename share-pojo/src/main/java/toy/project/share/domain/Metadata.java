package toy.project.share.domain;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import toy.project.share.util.JsonUtil;

@NoArgsConstructor
public class Metadata extends HashMap<String, String> {
    public Metadata(Map<String, String> metadata) {
        super(metadata);
    }

    public static Metadata fromJson(String json) {
        return JsonUtil.fromJson(json, Metadata.class);
    }

    public static Metadata ofItem(MetadataItem... metadataItems) {
        return new Metadata(
            Arrays.stream(metadataItems)
                .collect(Collectors.toMap(MetadataItem::getKey, MetadataItem::getValue))
        );
    }

    public static Metadata ofList(List<MetadataItem> metadataItems) {
        return new Metadata(
            metadataItems.stream()
                .collect(Collectors.toMap(MetadataItem::getKey, MetadataItem::getValue))
        );
    }

    public static Metadata empty() {
        return new Metadata();
    }

    public static Metadata sample() {
        return ofItem(
            new MetadataItem("location", "New York"),
            new MetadataItem("marriage", "Y")
        );
    }

    @JsonIgnore
    public void setValues(NameValueList nameValues) {
        boolean upsert = Boolean.parseBoolean(nameValues.getValueOf("upsert"));
        Metadata newMetadata = Metadata.fromJson(nameValues.getValueOf("metadata"));
        newMetadata.entrySet().stream()
            .filter(entry -> upsert || containsKey(entry.getKey()))
            .forEach(metadataItem -> put(metadataItem.getKey(), metadataItem.getValue()));
    }

    public Metadata filter(IdList keys) {
        if (keys.isEmpty()) {
            return this;
        } else {
            return new Metadata(
                entrySet().stream()
                    .filter(entry -> keys.contains(entry.getKey()))
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue))
            );
        }
    }

    public List<MetadataItem> toList() {
        return entrySet().stream()
            .map(entry -> new MetadataItem(entry.getKey(), entry.getValue()))
            .collect(Collectors.toList());
    }
}
