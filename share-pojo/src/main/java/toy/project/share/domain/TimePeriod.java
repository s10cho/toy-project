package toy.project.share.domain;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Getter;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
@AllArgsConstructor
public class TimePeriod {
    //
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    private final String zoneId;

    private final Long startTime;

    private final Long endTime;

    public TimePeriod() {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startTime = System.currentTimeMillis();
        this.endTime = null;
    }

    public TimePeriod(long startTime, long endTime) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimePeriod(ZonedDateTime startTime, ZonedDateTime endTime) {
        //
        this.zoneId = startTime.getZone().getId();
        this.startTime = startTime.toInstant().toEpochMilli();
        this.endTime = endTime.toInstant().toEpochMilli();
    }

    public TimePeriod(LocalDateTime startTime, LocalDateTime endTime) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startTime = startTime.atZone(ZoneId.of(zoneId)).toInstant().toEpochMilli();
        this.endTime = endTime.atZone(ZoneId.of(zoneId)).toInstant().toEpochMilli();
    }

    public TimePeriod(long startTime) {
        //
        this(startTime, 0L);
    }

    public static TimePeriod fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TimePeriod.class);
    }

    public static TimePeriod sample() {
        //
        return new TimePeriod(
            LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
            LocalDateTime.now().plusDays(10).toEpochSecond(ZoneOffset.UTC)
        );
    }

    public static void main(String[] args) {
        //
        SamplePrinter.println(sample());
        SamplePrinter.println(sample().toSimpleString());
    }

    public String toSimpleString() {
        //
        return String.format("StartTime[%s], EndTime[%s]", getStartTimeString(), getEndTimeString());
    }

    public LocalDateTime getStartLocalDateTime() {
        //
        return Instant.ofEpochMilli(startTime)
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime();
    }

    public ZonedDateTime getStartZonedDateTime() {
        //
        return Instant.ofEpochMilli(startTime)
            .atZone(ZoneId.systemDefault());
    }

    public LocalDateTime getEndLocalDateTime() {
        //
        return Instant.ofEpochMilli(endTime)
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime();
    }

    public ZonedDateTime getEndZonedDateTime() {
        //
        return Instant.ofEpochMilli(endTime)
            .atZone(ZoneId.systemDefault());
    }

    public String getStartTimeString() {
        //
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(startTime);
        return new SimpleDateFormat(DEFAULT_TIME_FORMAT).format(cal.getTime());
    }

    public String getEndTimeString() {
        //
        if (endTime == null || endTime == 0L) {
            return "00:00:00";
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(startTime);
        return new SimpleDateFormat(DEFAULT_TIME_FORMAT).format(cal.getTime());
    }
}
