package toy.project.share.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NameValue {
    private String name;

    private String value;

    public static NameValue fromJson(String json) {
        return JsonUtil.fromJson(json, NameValue.class);
    }

    public static NameValue sample() {
        return new NameValue("name", "Cheolsoo Kim");
    }

    public static void main(String[] args) {
        SamplePrinter.println(sample());
        SamplePrinter.println(sample().toSimpleString());
    }

    public String toSimpleString() {
        return String.format("%s:%s", name, value);
    }
}
