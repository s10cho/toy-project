package toy.project.share.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
@AllArgsConstructor
public class IdNameList {
    //
    private List<IdName> idNames;

    public IdNameList() {
        //
        this.idNames = new ArrayList<>();
    }

    public IdNameList(IdName idName) {
        //
        this();
        this.idNames.add(idName);
    }

    public IdNameList(String id, String name) {
        //
        this();
        this.idNames.add(new IdName(id, name));
    }

    public static IdNameList fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IdNameList.class);
    }

    public static IdNameList sample() {
        //
        return new IdNameList(IdName.sample());
    }

    public static void main(String[] args) {
        //
        SamplePrinter.println(sample());
    }

    public void add(IdName idName) {
        //
        this.idNames.add(idName);
    }

    public void add(String id, String name) {
        //
        this.idNames.add(new IdName(id, name));
    }

    public void addAll(List<IdName> idNames) {
        //
        this.idNames.addAll(idNames);
    }

    public List<IdName> list() {
        //
        return idNames;
    }

    public boolean containsName(String name) {
        //
        for (IdName idName : this.idNames) {
            if (name.equals(idName.getName())) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        return idNames.size();
    }
}
