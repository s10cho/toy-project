package toy.project.share.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Tuple<A, B> {
    @SuppressWarnings("squid:S1948")
    private A a;

    @SuppressWarnings("squid:S1948")
    private B b;

    public static Tuple<String, String> split(String source, String token) {
        if (source.contains(token)) {
            return new Tuple<>(
                source.substring(0, source.indexOf(token)),
                source.substring(source.indexOf(token) + 1)

            );
        } else {
            throw new IllegalArgumentException("Split not allowed: " + source);
        }
    }
}
