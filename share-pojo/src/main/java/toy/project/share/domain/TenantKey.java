package toy.project.share.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@EqualsAndHashCode
public class TenantKey {
    private String tenantId;

    public TenantKey(TenantKey tenantKey) {
        this(tenantKey.getTenantId());
    }

    public static TenantKey sample() {
        return new TenantKey("spectra");
    }

    @JsonIgnore
    public TenantKey getTenantKey() {
        return new TenantKey(this);
    }
}
