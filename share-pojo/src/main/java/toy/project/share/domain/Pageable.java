package toy.project.share.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Pageable {
    private int offset;

    private int limit;

    public static Pageable all() {
        return new Pageable(0, 999);
    }

    public int getNextOffset() {
        return offset + limit;
    }

    public int getNextOffset(long total) {
        if (total <= getNextOffset()) {
            return -1;
        } else {
            return getNextOffset();
        }
    }
}
