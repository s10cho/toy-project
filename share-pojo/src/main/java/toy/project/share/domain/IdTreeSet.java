package toy.project.share.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@NoArgsConstructor
public class IdTreeSet extends TreeSet<String> {

    public IdTreeSet(Collection<? extends String> c) {
        super(c);
    }

    public static IdTreeSet fromJson(String json) {
        return JsonUtil.fromJson(json, IdTreeSet.class);
    }

    public static IdTreeSet empty() {
        return new IdTreeSet();
    }

    public static IdTreeSet of(String... ids) {
        return new IdTreeSet(Arrays.asList(ids));
    }

    public static IdTreeSet sample() {
        return IdTreeSet.of("Doris", "Craig", "Young");
    }

    public static void main(String[] args) {
        SamplePrinter.println(sample());
    }

    public IdTreeSet with(String id) {
        if (StringUtils.isNotBlank(id)) {
            add(id);
        }
        return this;
    }

    public IdTreeSet filter(IdTreeSet filter) {
        return filter.stream()
            .filter(this::contains)
            .collect(Collectors.toCollection(IdTreeSet::new));
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public IdList toList() {
        return new IdList(this);
    }
}
