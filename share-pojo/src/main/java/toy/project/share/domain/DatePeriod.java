package toy.project.share.domain;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.NoSuchElementException;
import java.util.Objects;

import lombok.Getter;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
public class DatePeriod {
    //
    private final String zoneId;

    private final String startDate;

    private final String endDate;

    public DatePeriod() {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        this.endDate = null;
    }

    public DatePeriod(LocalDate startDate) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startDate = startDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        this.endDate = null;
    }

    public DatePeriod(LocalDate startDate, LocalDate endDate) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startDate = startDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        this.endDate = endDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public DatePeriod(ZoneId zoneId, LocalDate startDate, LocalDate endDate) {
        //
        this(zoneId.getId(), startDate, endDate);
    }

    public DatePeriod(String zoneId, LocalDate startDate, LocalDate endDate) {
        //
        this.zoneId = zoneId;
        this.startDate = startDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        if (endDate == null) {
            this.endDate = null;
        } else {
            this.endDate = endDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        }
    }

    public DatePeriod(String zoneId, String startDate, String endDate) {
        //
        this.zoneId = zoneId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public DatePeriod(LocalDate startDate, int days) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startDate = startDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        this.endDate = startDate.plusDays(days).format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public DatePeriod(String startDateStr, int days) {
        //
        this.zoneId = ZoneId.systemDefault().getId();
        this.startDate = startDateStr;
        LocalDate start = LocalDate.parse(startDateStr);
        this.endDate = start.plusDays(days).format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static DatePeriod sample() {
        //
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(10L);

        return new DatePeriod(zoneId, startDate, endDate);
    }

    public static DatePeriod fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DatePeriod.class);
    }

    public static void main(String[] args) {
        //
        SamplePrinter.println(sample());

        LocalDate now = LocalDate.now();
        LocalDate yesterday = now.minusDays(1);
        LocalDate tomorrow = now.plusDays(1);

        DatePeriod datePeriod = new DatePeriod(ZoneId.systemDefault().getId(), yesterday, tomorrow);

        SamplePrinter.println(datePeriod);
        SamplePrinter.println(datePeriod.contains(LocalDate.now()));
        SamplePrinter.println(datePeriod.contains(LocalDate.now().minusDays(1)));
        SamplePrinter.println(datePeriod.contains(LocalDate.now().minusDays(2)));
        SamplePrinter.println(datePeriod.contains(LocalDate.now().plusDays(1)));
        SamplePrinter.println(datePeriod.contains(LocalDate.now().plusDays(2)));

    }

    public LocalDate getStartLocalDate() {
        //
        return LocalDate.parse(startDate);
    }

    public LocalDate getEndLocalDate() {
        //
        if (endDate == null) {
            throw new NoSuchElementException("No endDate value.");
        }

        return LocalDate.parse(endDate);
    }

    public boolean hasEndDate() {
        //
        boolean hasEndDate = false;
        if (this.endDate != null) {
            hasEndDate = true;
        }
        return hasEndDate;
    }

    public int getStartYear() {
        //
        if (startDate == null) {
            return 0;
        }
        LocalDate start = LocalDate.parse(startDate);
        return start.getYear();
    }

    public int getStartMonthValue() {
        //
        if (startDate == null) {
            return 0;
        }
        LocalDate start = LocalDate.parse(startDate);
        return start.getMonthValue();
    }

    public int getEndYear() {
        //
        if (endDate == null) {
            return 0;
        }
        LocalDate end = LocalDate.parse(endDate);
        return end.getYear();
    }

    public int getEndMonthValue() {
        //
        if (endDate == null) {
            return 0;
        }
        LocalDate end = LocalDate.parse(endDate);
        return end.getMonthValue();
    }

    public boolean contains(LocalDate date) {
        //
        if (!hasEndDate()) {
            return false;
        }

        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);

        return (start.isEqual(date) || start.isBefore(date)) && (end.isEqual(date) || end.isAfter(date));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final DatePeriod datePeriod = (DatePeriod) o;

        return Objects.equals(zoneId, datePeriod.zoneId)
            && Objects.equals(startDate, datePeriod.startDate)
            && Objects.equals(endDate, datePeriod.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zoneId, startDate, endDate);
    }
}
