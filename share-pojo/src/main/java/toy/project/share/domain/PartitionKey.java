package toy.project.share.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(callSuper = true)
public class PartitionKey extends TenantKey {
    private String partitionId;

    public PartitionKey(PartitionKey partitionKey) {
        this(
            partitionKey.getTenantKey(),
            partitionKey.getPartitionId()
        );
    }

    public PartitionKey(TenantKey tenantKey, String partitionId) {
        super(tenantKey);
        this.partitionId = partitionId;
    }

    public PartitionKey(String tenantId, String partitionId) {
        this(new TenantKey(tenantId), partitionId);
    }

    public static PartitionKey sample() {
        return new PartitionKey(TenantKey.sample(), "NO PARTITION");
    }

    @JsonIgnore
    public PartitionKey getPartitionKey() {
        return new PartitionKey(this);
    }
}
