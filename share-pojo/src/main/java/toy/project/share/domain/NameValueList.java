package toy.project.share.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.databind.JsonSerializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
@AllArgsConstructor
public class NameValueList {
    private final List<NameValue> nameValues;

    public NameValueList() {
        this.nameValues = new ArrayList<>();
    }

    public NameValueList(NameValue nameValue) {
        this();
        this.nameValues.add(nameValue);
    }

    public NameValueList(String name, String value) {
        this();
        this.nameValues.add(new NameValue(name, value));
    }

    public NameValueList(NameValueList nameValues) {
        this();
        for (NameValue nameValue : nameValues.list()) {
            this.nameValues.add(new NameValue(nameValue.getName(), nameValue.getValue()));
        }
    }

    public static NameValueList newInstance(String name, String value) {
        return new NameValueList(name, value);
    }

    public static NameValueList newInstance(String name, long value) {
        return new NameValueList(name, Long.toString(value));
    }

    public static NameValueList newInstance(String name, boolean value) {
        return new NameValueList(name, Boolean.toString(value));
    }

    public static NameValueList newInstance(String name, Enum value) {
        return new NameValueList(name, value.name());
    }

    public static NameValueList newInstance(String name, Objects value) {
        return new NameValueList(name, JsonUtil.toJson(value));
    }

    public static NameValueList sample() {
        return new NameValueList("name", "Cheolsoo Kim");
    }

    public static NameValueList fromJson(String json) {
        return JsonUtil.fromJson(json, NameValueList.class);
    }

    public static void main(String[] args) {
        SamplePrinter.println(sample());
    }

    public NameValueList add(NameValue nameValue) {
        this.nameValues.add(nameValue);
        return this;
    }

    public NameValueList add(String name, String value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, value));
        }
        return this;
    }

    public NameValueList add(String name, Boolean value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, Boolean.toString(value)));
        }
        return this;
    }

    public NameValueList add(String name, Integer value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, Integer.toString(value)));
        }
        return this;
    }

    public NameValueList add(String name, Long value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, Long.toString(value)));
        }
        return this;
    }

    public NameValueList add(String name, Enum value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, value.name()));
        }
        return this;
    }

    public NameValueList add(String name, Objects value) {
        if (value != null) {
            this.nameValues.add(new NameValue(name, JsonUtil.toJson(value)));
        }
        return this;
    }

    public NameValueList remove(String name) {
        NameValue targetNameValue = null;
        for (NameValue nameValue : nameValues) {
            if (nameValue.getName().equals(name)) {
                targetNameValue = nameValue;
                break;
            }
        }
        if (targetNameValue != null) {
            nameValues.remove(targetNameValue);
        }

        return this;
    }

    public String getValueOf(String name) {
        return getNameValue(name).getValue();
    }

    public NameValue getNameValue(String name) {
        return this.nameValues
            .stream()
            .filter(nameValue -> name.equals(nameValue.getName()))
            .findFirst()
            .orElse(null);
    }

    public void addAll(List<NameValue> nameValues) {
        this.nameValues.addAll(nameValues);
    }

    public List<NameValue> list() {
        return nameValues;
    }

    public boolean containsName(String name) {
        return nameValues
            .stream()
            .anyMatch(nv -> nv.getName().equals(name));
    }

    public int size() {
        return nameValues.size();
    }

    public static NameValueList of(NameValue... nameValues) {
        return new NameValueList(Arrays.asList(nameValues));
    }
}
