package toy.project.share.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.StreamLib;

@NoArgsConstructor
public class IdList extends ArrayList<String> {

    public IdList(Collection<? extends String> c) {
        super(c);
    }

    public static IdList fromJson(String json) {
        return JsonUtil.fromJson(json, IdList.class);
    }

    public static IdList empty() {
        return new IdList();
    }

    public static IdList of(String... ids) {
        return new IdList(Arrays.asList(ids));
    }

    public static IdList sample() {
        return IdList.of("Doris", "Craig", "Young");
    }

    public IdList with(String id) {
        if (StringUtils.isNotBlank(id)) {
            add(id);
        }
        return this;
    }

    public IdList filter(IdList filter) {
        return filter.stream()
            .filter(this::contains)
            .collect(Collectors.toCollection(IdList::new));
    }

    public IdList without(IdList without) {
        return stream()
            .filter(StreamLib.not(without::contains))
            .collect(Collectors.toCollection(IdList::new));
    }

    public IdList without(String without) {
        return stream()
            .filter(StreamLib.not(without::equals))
            .collect(Collectors.toCollection(IdList::new));
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    @Override
    public String[] toArray() {
        return toArray(new String[size()]);
    }
}
