package toy.project.share.domain;

import java.util.Objects;
import java.util.StringTokenizer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import toy.project.share.util.JsonUtil;
import toy.project.share.util.SamplePrinter;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IdName {
    //
    private String id;

    private String name;

    public static IdName fromString(String idNameStr) {
        //
        StringTokenizer tokenizer = new StringTokenizer(idNameStr, ":");
        String id = tokenizer.nextToken();
        String name = tokenizer.nextToken();

        return new IdName(id, name);
    }

    public static IdName sample() {
        //
        String id = "1234";
        String name = "Hansoo Lee";

        return new IdName(id, name);
    }

    public static IdName fromJson(String json) {
        //
        return JsonUtil.fromJson(json, IdName.class);
    }

    public static void main(String[] args) {
        //
        SamplePrinter.println(sample());
        SamplePrinter.println(IdName.fromString(sample().toSimpleString()));
    }

    public String toSimpleString() {
        //
        return id + ":" + name;
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        IdName idName = (IdName) target;

        return Objects.equals(id, idName.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
