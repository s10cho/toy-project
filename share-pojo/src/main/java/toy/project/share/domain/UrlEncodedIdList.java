package toy.project.share.domain;

import lombok.NoArgsConstructor;
import toy.project.share.util.UrlLib;

@NoArgsConstructor
public class UrlEncodedIdList extends IdList {
    @Override
    public boolean add(String s) {
        return super.add(UrlLib.decode(s));
    }

    @Override
    public IdList with(String id) {
        return super.with(UrlLib.decode(id));
    }
}
