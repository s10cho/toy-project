package toy.project.share.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import toy.project.share.util.SamplePrinter;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Node<T> {
    @JsonManagedReference
    private final List<Node<T>> children = new ArrayList<>();

    @JsonBackReference
    private Node<T> parent;

    @SuppressWarnings("squid:S1948")
    private T data;

    public Node(T data) {
        this.data = data;
    }

    public static Node<String> sample() {
        Node<String> sample = new Node<>("ROOT");
        sample.addChild(new Node<>("상담하기"));
        sample.addChild(new Node<>("대시보드 & 리포트"));
        Node<String> environment = sample.addChild(new Node<>("운영설정"));
        environment.addChild(new Node<>("센터운영"));
        environment.addChild(new Node<>("사용자"));
        return sample;
    }

    public static void main(String[] args) {
        SamplePrinter.println(sample());
    }

    private void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public Node<T> addChild(Node<T> child) {
        child.setParent(this);
        this.children.add(child);
        return child;
    }

    public void addChildren(List<Node<T>> children) {
        children.forEach(each -> each.setParent(this));
        this.children.addAll(children);
    }
}
